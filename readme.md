## Assistant

This project utilizes openai assistant. Currently functions are added as a predefined parameter.

# Run the project

### Create Virtual environment

- Ubuntu/linux

```sh
python3 -m venv venv
```

- Windows

```cmd
python -m venv venv
```

### activate virtual environment

- Ubuntu/linux

```bash
source venv/bin/activate
```

- window

```ps
\venv\Scripts\activate.bat
```

### Install requirements

```cmd
pip install -r requirements.txt
```

### update .env file

```sh
cp .env.example .env
```

- SENDER - email address
- PASSWORD - password of email address
- OPEN_AI_API_KEY - open ai key

### run project

```cmd
python main.py
```

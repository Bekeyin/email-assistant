import json

from dotenv import load_dotenv

from send_mail import send_mail

load_dotenv()

from ai_client import AiAssistant

# AiAssistant.all_assitants()
# assistant = AiAssistant.from_assistant_id("asst_W7JIvfSSeKDq5PBq8AwIRkNC")
# assistant = AiAssistant.from_parameter_input_for_email()
id = input("Enter Assistant Id: ")

assistant = AiAssistant.from_assistant_id(id)
response = ""
while True:
    message = input("User: ")
    if message == "quit" or message == "exit":
        assistant.__file_handler.write(assistant.thread_messages)
        break
    assistant.run(message=message)
    is_function, response = assistant.check_if_function()
    if not is_function:
        print("Assistant: ", response.data[0].content[0].text.value)
    else:
        tools_outputs = []
        for data in response:
            function_name = data["function"]
            args = data["arguments"]
            if function_name == "send_mail":
                print("Sending mail ... ... ... ..")
                res = send_mail(
                    args.get("reciever", None),
                    args.get("subject", None),
                    args.get("message", None),
                )
            else:
                res = {"status": "Failed", "message": "Function not found."}

            tools_outputs.append(
                {"tool_call_id": data["id"], "output": json.dumps(res)}
            )
        is_function, response = assistant.send_tool_call_response(tools_outputs)
        print("Assistant: ", response.data[0].content[0].text.value)
print("Thank you for using Assistant")
quit()

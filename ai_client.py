import json
import os
import sys
import time
from io import BufferedReader

from openai import OpenAI
from openai.types.beta.threads import ThreadMessage

from ai_functions import SEND_MAIL
from json_handler import JsonFileHandler
from type import MODELS, TOOLS, Function, ThreadMetaData, ToolOutput

# Generating client
client = OpenAI(api_key=os.environ.get("OPEN_AI_API_KEY"), max_retries=3, timeout=180)


DEFAULT_THREAD_META_DATA = ThreadMetaData(user="bikin", modified=False)


class OpenAiFiles:
    __client = client
    __json_file_handler = JsonFileHandler()

    def __init__(self, to_dict=False):
        self.to_dict = to_dict

    def get_files(self):
        response = self.__client.files.list()
        self.__json_file_handler.write(
            [json.loads(data.model_dump_json()) for data in response.data]
        )
        if self.to_dict:
            return json.loads(response.model_dump_json())
        return response.data

    def get_file(self, id):
        response = self.__client.files.retrieve(id)
        if self.to_dict:
            return json.loads(response.model_dump_json())
        return response

    def upload_file(self, file: BufferedReader | str, purpose="assistant"):
        if isinstance(file, str):
            if os.path.exists(file):
                _file = open(file, "rb")
            else:
                raise ValueError("File Does not exists.")
        elif isinstance(file, BufferedReader):
            _file = file
        else:
            raise ValueError("Invalid File type.")
        response = self.__client.files.create(file=_file, purpose=purpose)
        self.__json_file_handler.append_data(json.loads(response.model_dump_json()))
        if self.to_dict:
            return json.loads(response.model_dump_json())
        return response

    def delete_file(self, id):
        response = self.__client.files.delete(id)
        self.get_files()
        if self.to_dict:
            return json.loads(response.model_dump_json())
        return response

    def set_dict(self, to_dict):
        self.to_dict = to_dict


class AiAssistant:
    __client = client
    __files = OpenAiFiles()
    __file_handler = JsonFileHandler("assistants.json")
    __messages_handler = JsonFileHandler("messages.json")

    def __init__(
        self,
    ):
        self.flag = False
        self.thread_messages = {"messages": []}
        pass

    @classmethod
    def from_parameter_input_for_email(cls):
        name = input("Enter the name of the Assistant you want to create: ")
        description = input("Enter your assistant description: ")
        instruction = input("Enter an instruction for your assistant: ")
        files = cls.__files.get_files()
        files_list = [json.loads(item.model_dump_json()) for item in files]
        file_names = [
            f"{index+1}: {data['id']}" for index, data in enumerate(files_list)
        ]
        choices = "\n".join(file_names)
        file = input(f"choose you file: \n{choices}\n want to create: press c")
        if file.lower() == "c":
            file_path = input("Enter your file path: ")
            response = cls.__files.upload_file(file_path)
            file_ids = [response.id]
        elif file.isdigit() and int(file) < len(file_names):
            file_ids = [files_list[int(file)]["id"]]
        else:
            file_ids = []
        function = Function(type="function", func=SEND_MAIL)
        instance = cls()
        instance.create_assistant(
            name,
            description,
            instruction,
            tool=function,
            file_id=file_ids,
        )
        instance.create_thread()
        return instance

    @classmethod
    def from_assistant_id(cls, assistant_id):
        instance = cls()
        instance.assistant = cls.__client.beta.assistants.retrieve(
            assistant_id=assistant_id
        )
        instance.create_thread(messages=[])
        return instance

    @classmethod
    def from_assistant_id_and_thread_id(cls, assistant_id, thread_id):
        instance = cls()
        instance.assistant = cls.__client.beta.assistants.retrieve(
            assistant_id=assistant_id
        )
        instance.get_thread(thread_id)
        return instance

    @classmethod
    def all_assitants(cls):
        response = cls.__client.beta.assistants.list()
        cls.__file_handler.write(
            [json.loads(item.model_dump_json()) for item in response.data]
        )
        return json.loads(response.model_dump_json())

    @property
    def file(self):
        return self.__files

    @property
    def file_handler(self):
        return self.__file_handler

    def create_assistant(
        self,
        name,
        description,
        instruction,
        tool: TOOLS | Function = "code_interpreter",
        model: MODELS = "gpt-4-1106-preview",
        file_id: list[str | None] = [],
    ):
        self.assistant = self.__client.beta.assistants.create(
            name=name,
            description=description,
            instructions=instruction,
            file_ids=file_id,
            tools=self.get_tool(tool),
            model=model,
        )

    def get_thread(self, thread_id):
        self.thread = self.__client.beta.threads.retrieve(thread_id=thread_id)
        self.thread_messages["thread_id"] = self.thread.id

    def add_files_to_assistant(self, file_path: str, to_dict=False):
        self.file.set_dict(to_dict)
        response = self.file.upload_file(file=file_path)
        response = self.__client.beta.assistants.files.create(
            assistant_id=self.assistant.id, file_id=response.id
        )
        if to_dict:
            return json.loads(response.model_dump_json())
        return response

    def remove_file_from_assistant(self, file_id: str, to_dict=False):
        response = self.__client.beta.assistants.files.delete(
            file_id=file_id, assistant_id=self.assistant.id
        )
        if to_dict:
            return json.loads(response.model_dump_json())
        return response

    def get_tool(self, tool: TOOLS | Function | None):
        if tool and type(tool) == TOOLS:
            tool = [{"type": tool}]
        elif tool and type(tool) == Function:
            tool = [{"type": "function", "function": tool.func}]
        else:
            tool = []
        return tool

    def modify_assistant(
        self,
        instruction: str,
        model: str = None,
        tool: TOOLS | Function | None = None,
        name: str = None,
        file_id: list[str] | None = [],
    ):
        if hasattr(self, "assistant"):
            self.assistant = self.__client.beta.assistants.update(
                self.assistant.id,
                instructions=instruction or self.assistant.instructions,
                model=model or self.assistant.model,
                tools=self.get_tool(tool),
                name=name or self.assistant.name,
                file_ids=file_id or self.assistant.file_ids,
            )
        else:
            raise ValueError("Please create Assistant before updating assistant.")

    def create_thread(
        self,
        messages: list[ThreadMessage] = [],
        metadata: ThreadMetaData = DEFAULT_THREAD_META_DATA,
    ):
        self.thread = self.__client.beta.threads.create(
            messages=messages, metadata=metadata
        )
        self.thread_messages["thread_id"] = self.thread.id

    def add_message(self, message):
        self.thread_messages["messages"].append({"user": message})
        self.message = self.__client.beta.threads.messages.create(
            thread_id=self.thread.id, role="user", content=message
        )
        self.__messages_handler.write(self.thread_messages)

    def run(self, message, instruction=None, model=None):
        self.flag = False
        self.add_message(message)
        print("Generating....", end="")
        self.runner = self.__client.beta.threads.runs.create(
            thread_id=self.thread.id,
            assistant_id=self.assistant.id,
            instructions=instruction or self.assistant.instructions,
            model=model or self.assistant.model,
        )
        self.wait_result()
        return self.runner

    def wait_result(self):
        while self.runner.status == "in_progress" or self.runner.status == "queued":
            self.runner = self.__client.beta.threads.runs.retrieve(
                thread_id=self.thread.id, run_id=self.runner.id
            )
            sys.stdout.write(".")
            time.sleep(0.5)
        print("\n")
        self.flag = True

    def get_result(self):
        self.messages = self.__client.beta.threads.messages.list(
            self.thread.id, order="desc"
        )
        data = self.messages
        self.thread_messages["messages"].append(
            {"assistant": data.data[0].content[0].text.value}
        )
        self.__messages_handler.write(self.thread_messages)
        return data

    def check_if_function(self):
        if self.runner.status == "requires_action":
            tool_calls = self.runner.required_action.submit_tool_outputs.tool_calls
            callables = [
                {
                    "id": call.id,
                    "function": call.function.name,
                    "arguments": json.loads(call.function.arguments),
                }
                for call in tool_calls
            ]
            self.thread_messages["messages"].append({"function_calling": callables})
            self.__messages_handler.write(self.thread_messages)
            return True, callables
        else:
            return False, self.get_result()

    def send_tool_call_response(self, output: list[ToolOutput]):
        self.runner = self.__client.beta.threads.runs.submit_tool_outputs(
            run_id=self.runner.id, thread_id=self.thread.id, tool_outputs=output
        )
        self.wait_result()
        return self.check_if_function()

    def get_value(self, response):
        return response.data[0].content[0].text.value

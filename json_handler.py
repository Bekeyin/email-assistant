import json


class JsonFileHandler:
    def __init__(self, file_path="files.json") -> None:
        self.file_path = file_path

    def get_file(self, mode="w"):
        self.file = open(self.file_path, mode=mode)

    def get_data(self):
        self.get_file(mode="r")
        data = json.load(fp=self.file)
        return data

    def append_data(self, data: dict):
        file_data = self.get_data(mode="a")
        if data:
            file_data.append(data)
        else:
            file_data = [data]
        json.dump(file_data, self.file)
        return file_data

    def write(self, data: list[dict]):
        self.get_file()
        json.dump(data, self.file)

import os
import smtplib
import ssl
import threading
from collections.abc import Callable, Iterable, Mapping
from email.message import Message
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import Any

from dotenv import load_dotenv

load_dotenv()

PORT = 587  # For starttls
HOST = "smtp.gmail.com"
SENDER = os.environ.get("SENDER")
PASSWORD = os.environ.get("PASSWORD")


def send_mail(reciever, subject, message):
    if reciever and subject and message:
        _message = MIMEMultipart("alternative")
        _message["Subject"] = subject
        _message["From"] = SENDER
        _message["To"] = reciever
        try:
            context = ssl.create_default_context()
            with smtplib.SMTP(host=HOST, port=PORT) as server:
                server.starttls(context=context)
                server.login(SENDER, PASSWORD)
                text = MIMEText(message, "plain")
                _message.attach(text)
                server.sendmail(SENDER, [reciever], _message.as_string())
            return {
                "status": "Email Send Successfully",
                "to": reciever,
            }
        except Exception as e:
            print("Shit: ", e)
            return {
                "status": "Failed",
                "message": "Server Error. Please try again later.",
            }
    else:
        return {
            "status": "Error",
            "error": "reciever,subject and message are required field.",
        }


def send_mail_test(reciever, subject, message):
    if reciever and subject and message:
        context = ssl.create_default_context()
        with smtplib.SMTP(host=HOST, port=PORT) as server:
            server.starttls(context=context)
            server.login(SENDER, PASSWORD)
            _message = f"""Subject: {subject}
    {message}
    """
            server.sendmail(SENDER, [reciever], _message)
        return {
            "status": "Email Send Successfully",
            "to": reciever,
        }
    else:
        return {
            "status": "Error",
            "error": "reciever,subject and message are required field.",
        }


class EmailThread(threading.Thread):
    def __init__(
        self,
        group: None = None,
        target: Callable[..., object] | None = None,
        name: str | None = None,
        args: Iterable[Any] = ...,
        kwargs: Mapping[str, Any] | None = None,
        *,
        daemon: bool | None = None,
    ) -> None:
        super().__init__(group, target, name, args, kwargs, daemon=daemon)

    def run(self, subject, message, reciever) -> None:
        send_mail(reciever=reciever, subject=subject, message=message)
        return "SEND"


send_mail("razzshrz07@gmail.com", "test", "test")
